import express from 'express';
import { calculation} from '../services/calculator';
var router = express.Router();
/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Express' });
});
router.post('/api/finance',  (req, res, next) => {
  res.json({ output: calculation(req.body) });
});
export default router;