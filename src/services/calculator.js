import moment from 'moment';
export class Calculator{
    input = {};
    constructor(input) {
        this.input = input;
    }
    getIRR = () => {
        let input = this.input;
        if(this.validateInput()){
        let netProfit = 0;
        let data = [];
        let initialInvestment = input.principal - input.upfrontFee.value;
        netProfit = input.upfrontFee.value - input.principal;
        data.push({ amount: netProfit, when: new Date('2016', '9', '20') });
        input.schedule.forEach((x, i) => {
            let installment = x.interestFee + x.principal;
            netProfit += installment;
        });
        return netProfit / initialInvestment;
    }
    return -1;

    };
    getAPR_Wikipedia = (IRR) => {
        let input = this.input;

        IRR = IRR || this.getIRR();
        if (this.validateInput()){
                return +Math.pow((1 + IRR + (input.upfrontFee.value / input.principal)), input.schedule.length).toFixed(1);
            }
        return -1;
    }

    getAPR_Investopedia = () => {
        let input = this.input;
        if (this.validateInput()) {

            let startDate = moment(input.schedule[0].date);
            let endDate = moment(input.schedule[input.schedule.length - 1].date);
            let numberOfDays = endDate.diff(startDate, 'days');

            let totalInterest = input.schedule.reduce((x, y) => x + (y.interestFee || 0), 0);
            
            return +((((input.upfrontFee.value + totalInterest) / input.principal) / numberOfDays) * 365 * 100).toFixed(1);
        }
        return -1;
    }
    validateInput = () => {
        let input = this.input;
        return (input &&
            input.principal && input.principal > 0 &&
            input.upfrontFee && input.upfrontFee.value >= 0 &&
            input.schedule && Array.isArray(input.schedule) && input.schedule.length >= 2)   
    }
}



export function calculation(input) {
    let calculator = new Calculator(input);
    let IRR = calculator.getIRR();

    return { IRR: IRR, 
                APR_investopedia: calculator.getAPR_Investopedia(), 
                APR_wikipedia: calculator.getAPR_Wikipedia(IRR) };
}
