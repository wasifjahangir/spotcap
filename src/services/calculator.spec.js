import chai from 'chai';
let expect = chai.expect;
import { Calculator} from './calculator';
let input = {};
beforeEach(function () {
    input = {
        "principal": 51020400,
        "upfrontFee": {
            "value": 1020400
        },
        "upfrontCreditlineFee": {
            "value": 0
        },
        "schedule": [
            {
                "id": 1,
                "date": "2016-10-20",
                "principal": 3595000,
                "interestFee": 1530600
            },
            {
                "id": 2,
                "date": "2016-11-21",
                "principal": 3702800,
                "interestFee": 1422800
            },
            {
                "id": 3,
                "date": "2016-12-20",
                "principal": 3813900,
                "interestFee": 1311700
            },
            {
                "id": 4,
                "date": "2017-01-20",
                "principal": 3928300,
                "interestFee": 1197300
            },
            {
                "id": 5,
                "date": "2017-02-20",
                "principal": 4046200,
                "interestFee": 1079400
            },
            {
                "id": 6,
                "date": "2017-03-20",
                "principal": 4167600,
                "interestFee": 958000
            },
            {
                "id": 7,
                "date": "2017-04-20",
                "principal": 4292600,
                "interestFee": 833000
            },
            {
                "id": 8,
                "date": "2017-05-22",
                "principal": 4421400,
                "interestFee": 704200
            },
            {
                "id": 9,
                "date": "2017-06-20",
                "principal": 4554000,
                "interestFee": 571600
            },
            {
                "id": 10,
                "date": "2017-07-20",
                "principal": 4690600,
                "interestFee": 435000
            },
            {
                "id": 11,
                "date": "2017-08-21",
                "principal": 4831400,
                "interestFee": 294200
            },
            {
                "id": 12,
                "date": "2017-09-20",
                "principal": 4976600,
                "interestFee": 149300
            }
        ]
    }

});


describe('Financial Calculations', function () {
    describe('IRR', function () {
        it('should return IRR when the input is valid', function () {
            let calculator = new Calculator(input);
            let _irr = calculator.getIRR();
            expect(0.23015).to.equal(_irr);
        });
        it('should return IRR if when the fee is zero', function () {
            input.upfrontFee.value = 0;
            let calculator = new Calculator(input);
            let _irr = calculator.getIRR();
            expect(_irr).to.greaterThan(0);
        });
        it('should return -1 if when the input is not valid', function () {
            let calculator = new Calculator(null);
            let _irr = calculator.getIRR();
            expect(-1).to.equal(_irr);
        });
        it('should return -1 if when the principal is not valid', function () {
            input.principal = 0
            let calculator = new Calculator(input);
            let _irr = calculator.getIRR();
            expect(-1).to.equal(_irr);
        });
        it('should return -1 if when the schedule is not valid', function () {
            input.schedule = null
            let calculator = new Calculator(input);
            let _irr = calculator.getIRR();
            expect(-1).to.equal(_irr);
        });
        it('should return -1 if when the schedule is not valid', function () {
            input.schedule = []
            let calculator = new Calculator(input);
            let _irr = calculator.getIRR();
            expect(-1).to.equal(_irr);
        });
        it('should return -1 if when the schedule is not valid', function () {
            input.schedule = [input.schedule[0]]
            let calculator = new Calculator(input);
            let _irr = calculator.getIRR();
            expect(-1).to.equal(_irr);
        });
        
    });
    describe('APR Wikipedia', function () {
        it('should return APR when the input is valid', function () {
            let calculator = new Calculator(input);
            let _irr = calculator.getIRR();
            let _apr = calculator.getAPR_Wikipedia(_irr);
            expect(14.6).to.equal(_apr);
        });
        it('should return APR when IRR is not specified', function () {
            let calculator = new Calculator(input);
            let _apr = calculator.getAPR_Wikipedia();
            expect(14.6).to.equal(_apr);
        });
        it('should return -1 if when the input is not valid', function () {
            let calculator = new Calculator(null);
            let _apr = calculator.getAPR_Wikipedia();
            expect(-1).to.equal(_apr);
        });
        it('should return -1 if when the principal is not valid', function () {
            input.principal = 0
            let calculator = new Calculator(input);
            let _apr = calculator.getAPR_Wikipedia();
            expect(-1).to.equal(_apr);
        });
        it('should return -1 if when the schedule is not valid', function () {
            input.schedule = null
            let calculator = new Calculator(input);
            let _apr = calculator.getAPR_Wikipedia();
            expect(-1).to.equal(_apr);
        });
        it('should return -1 if when the schedule is not valid', function () {
            input.schedule = 'foo bar';
            let calculator = new Calculator(input);
            let _apr = calculator.getAPR_Wikipedia();
            expect(-1).to.equal(_apr);
        });
        it('should return -1 if when the schedule is not valid', function () {
            input.schedule = [];
            let calculator = new Calculator(input);
            let _apr = calculator.getAPR_Wikipedia();
            expect(-1).to.equal(_apr);
        });
        it('should return -1 if when the schedule length less than 2', function () {
            input.schedule = [input.schedule[0]];
            let calculator = new Calculator(input);
            let _apr = calculator.getAPR_Wikipedia();
            expect(-1).to.equal(_apr);
        });
        
    });

    describe('APR Investopedia', function () {
        it('should return APR when the input is valid', function () {
            let calculator = new Calculator(input);
            let _apr = calculator.getAPR_Investopedia();
            expect(24.6).to.equal(_apr);
        });
        
        it('should return -1 if when the input is not valid', function () {
            let calculator = new Calculator(null);
            let _apr = calculator.getAPR_Wikipedia();
            expect(-1).to.equal(_apr);
        });
        it('should return -1 if when the principal is not valid', function () {
            input.principal = 0
            let calculator = new Calculator(input);
            let _apr = calculator.getAPR_Wikipedia();
            expect(-1).to.equal(_apr);
        });
        it('should return -1 if when the schedule is not valid', function () {
            input.schedule = null
            let calculator = new Calculator(input);
            let _apr = calculator.getAPR_Wikipedia();
            expect(-1).to.equal(_apr);
        });
        it('should return -1 if when the schedule is not valid', function () {
            input.schedule = []
            let calculator = new Calculator(input);
            let _apr = calculator.getAPR_Wikipedia();
            expect(-1).to.equal(_apr);
        });
        it('should return -1 if when the schedule is not valid', function () {
            input.schedule = [input.schedule[0]]
            let calculator = new Calculator(input);
            let _apr = calculator.getAPR_Wikipedia();
            expect(-1).to.equal(_apr);
        });
        it('should return -1 if when the schedule is not valid', function () {
            input.schedule = 'foo:bar';
            let calculator = new Calculator(input);
            let _apr = calculator.getAPR_Wikipedia();
            expect(-1).to.equal(_apr);
        });

    });
});
